<?php
session_start();

// Verificar si el usuario ya ha iniciado sesión
if (isset($_SESSION['email'])) {
    header("Location: principal.php");
    exit();
}

// Verificar si se envió el formulario de inicio de sesión
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    require_once 'db_config.php';

    $email = $_POST['email'];
    $password = $_POST['password'];

    // Verificar las credenciales del usuario en la base de datos
    $conn = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_PASSWORD);
    $stmt = $conn->prepare("SELECT * FROM usuarios WHERE email = :email");
    $stmt->bindParam(':email', $email);
    $stmt->execute();
    $user = $stmt->fetch(PDO::FETCH_ASSOC);

    if ($user && password_verify($password, $user['password'])) {
        // Iniciar sesión y redireccionar al usuario a la página principal
        $_SESSION['email'] = $user['email'];
        $_SESSION['nombre'] = $user['nombre'];
        header("Location: principal.php");
        exit();
    } else {
        // Mostrar mensaje de error si las credenciales son incorrectas
        header("Location: error.php");
        exit();
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Iniciar sesión</title>
</head>
<body>
    <h2>Iniciar sesión</h2>
    <form method="POST">
        <input type="email" name="email" placeholder="Correo electrónico" required><br>
        <input type="password" name="password" placeholder="Contraseña" required><br>
        <input type="submit" value="Iniciar sesión">
    </form>
    <p>No tienes una cuenta? <a href="registrarse.php">Regístrate</a></p>
</body>
</html>
