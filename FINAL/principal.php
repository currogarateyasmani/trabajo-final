<?php
session_start();

// Verificar si el usuario ha iniciado sesión
if (!isset($_SESSION['email'])) {
    header("Location: index.php");
    exit();
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Página principal</title>
    <style>
        .logout-link {
            color: red;
        }
    </style>
</head>
<body>
    <h2>Bienvenido, <?php echo $_SESSION['nombre']; ?>!</h2>
    <p><a class="logout-link" href="logout.php">Cerrar sesión</a></p>
</body>
</html>
